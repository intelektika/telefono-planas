package objects;

import java.util.ArrayList;
import java.util.List;

public class Agent {
    private final List<Phone_Plan> phonePlanList;

    public Agent() {
        phonePlanList = new ArrayList<>();
    }

    public List<Phone_Plan> getPhonePlanList() { return phonePlanList; }

    public void addPhonePlan(String name, int cost, int messageLimit) {
        Phone_Plan temp = new Phone_Plan();
        temp.setName(name);
        temp.setCost(cost);
        temp.setMessage_Limit(messageLimit);
        phonePlanList.add(temp);
    }

    private double calculateResult(int value, int target) {
        return Math.pow(value - target, 2);
    }

    public void printPlans() {
        if(phonePlanList.isEmpty()) {
            System.out.println("Phone plan list is empty, unable to give suggestion!");
            return;
        }

        System.out.println("Plan list:");
        for(Phone_Plan target : getPhonePlanList()) {
            System.out.println("\t" + target.getName() + ": cost - " + target.getCost() + ", limit - " + target.getMessage_Limit() + ";");
        }
    }

    public void getSuggestion(int targetCost, int targetMessageLimit) {
        if(phonePlanList.isEmpty()) {
            System.out.println("Phone plan list is empty, unable to give suggestion!");
            return;
        }

        Suggestion suggestion = null;

        System.out.println("Desired cost - " + targetCost + ".\r\nDesired message limit - " + targetMessageLimit + ".\r\nCalculations:");
        for(int i = 0; i < getPhonePlanList().size(); i++) {
            double cost = calculateResult(getPhonePlanList().get(i).getCost(), targetCost);
            double limit = calculateResult(getPhonePlanList().get(i).getMessage_Limit(), targetMessageLimit);
            double answer = Math.sqrt(cost + limit);
            
            if(suggestion == null) {
                suggestion = new Suggestion(i, answer);
            } else if (suggestion.getAnswer() > answer) {
                suggestion.setId(i);
                suggestion.setAnswer(answer);
            }

            System.out.println("\tPhone plan (" + getPhonePlanList().get(i).getName() + "), answer = " + answer);
        }

        assert suggestion != null;
        System.out.println("\r\nSuggestion:\r\n\tIndex - " + suggestion.getId() + "\r\n\tAnswer - " + suggestion.getAnswer() + "\r\n");
        System.out.println(getPhonePlanList().get(suggestion.getId()).toString());
    }
}
