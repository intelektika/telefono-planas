package objects;
public class Phone_Plan {
    private String name;
    private int cost;
    private int message_Limit;

    public Phone_Plan() {}

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public int getCost() { return cost; }
    public void setCost(int cost) { this.cost = cost; }

    public int getMessage_Limit() { return message_Limit; }
    public void setMessage_Limit(int message_Limit) { this.message_Limit = message_Limit; }

    @Override
    public String toString() {
        return "Phone_Plan{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", message_Limit=" + message_Limit +
                '}';
    }
}
