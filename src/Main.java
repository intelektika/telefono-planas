import objects.Agent;

public class Main {
    public static void main(String[] args) {
        Agent agent = new Agent();
        agent.addPhonePlan("P1", 40, 1000);
        agent.addPhonePlan("P2", 5, 100);
        agent.addPhonePlan("P3", 100, 2000);

        agent.printPlans();
        System.out.println();
        agent.getSuggestion(40, 1000);
    }
}